# Github! Search

# Apresentação

Este projeto foi criado despretensioamente como teste para avaliação de perfil técnico.
Foi utilizado builder [Create React App](https://github.com/facebook/create-react-app).

## Instalação

- Faça um clone do projeto ou download dos arquivos;
- Pelo powershell, Win cdm ou Terminal, navegue até o diretório raiz do projeto.
- Instale as bibliotecas utilizando o comando `npm install`.
- Rode um ambiente de desenvolvimento com o comando `npm start`;
- Crie um build de produção com o comando `npm run build`;

Para outros comandos, consulte a documentação: [Create React App Docs](https://facebook.github.io/create-react-app/docs/getting-started).

## Observações

Foram utilizados as tecnologias React, Redux e Typescript; e a biblioteca Material-UI para a construção da interface.
Tenho a necessidade de aprender mais sobre essas tecnologias e a escolha pala Material se deve ao curto tempo disponível para esse projeto. A utilização de uma biblioteca otimiza bastante a programação, além de auxiliar a criação de layout, o que não é meu forte.

### Material UI

Segui as principais recomendações da biblioteca, utilizando o modelo de temeficação proprio, em vez de reescrever componentes. Atuei na raiz do theme central, customizando o que achei necessário. Valeu como estudo pra entender um pouco mais da forma de pensar dos desenvolvedores da Lib.

### Redux

O controle de estado com Redux foi utilizado apenas para controlar o Menu e disparar o loading global, usando um interceptor no Axios.

Particularmente não vi a necessidade de controlar os estados das buscas e resultados com Redux. A construção dele é bem simples, sendo fácil manter os dados atualizados utilizando react-router e parâmetros de url. Gosto de dar ao usuario a habilidade de digitar na url para refazer uma busca ou para selecionar parametros de filtro, replicar isso para o redux criaria uma complexidade desnecessária, na minha opinião.

O controle do menu e o estado do loading tbm poderiam ser feitos com Hooks, mas foi divertido utilizar o Redux com Typescript, algo novo pra mim.
