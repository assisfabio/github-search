import React, { FunctionComponent } from "react";
import {
  Button,
  ButtonProps,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";

export const CustomButton: FunctionComponent<ButtonProps> = (props) => {
  const { children } = props;
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("sm"));

  return (
    <Button size={matches ? "medium" : "large"} {...props}>
      {children}
    </Button>
  );
};

export default CustomButton;
