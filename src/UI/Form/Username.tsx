import React, { FunctionComponent, useEffect, useState } from "react";
import { AccountCircleOutlined } from "@material-ui/icons";
import CustomInput from "./CustomInput";
import { TextFieldProps } from "@material-ui/core";

export const Username: FunctionComponent<TextFieldProps> = ({
  id,
  label,
  error,
  value,
  onBlur,
  onFocus,
}) => {
  const [hasError, setHasError] = useState<boolean>(false);

  const handleChange = (e: any) => {
    const { currentTarget } = e;
    setHasError(currentTarget.value.length < 1);
  };

  useEffect(() => {
    setHasError(Boolean(error));
  }, [error]);

  return (
    <CustomInput
      required
      label={label}
      id={id}
      error={hasError}
      helperText={(hasError && `${label} inválido`) || undefined}
      StartIcon={AccountCircleOutlined}
      onChange={handleChange}
      value={value}
      onBlur={onBlur}
      onFocus={onFocus}
    />
  );
};

export default Username;
