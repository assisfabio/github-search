import React, { FunctionComponent } from "react";

export const Form: FunctionComponent<any> = ({
  children,
  onSubmit,
  ...props
}) => {
  const handleSubmit = (e: any) => {
    e.preventDefault();
    const { currentTarget } = e;
    const allFields = Array.from(currentTarget?.elements);
    const invalidFields = allFields.filter((i: any) => {
      return (
        i.attributes["aria-invalid"]?.value === "true" ||
        (i.required && (!i.value || i.value.trim() === ""))
      );
    });

    onSubmit(e, {
      invalid: !!invalidFields.length,
      valid: !invalidFields.length,
      invalidFields: invalidFields.map((item: any) => ({
        id: item.id,
        value: item.value?.trim(),
      })),
      fields: [
        ...allFields
          .filter((item: any) => {
            return item.id && item.id !== "";
          })
          .map((item: any) => ({
            id: item.id,
            value: item.value?.trim(),
          })),
      ],
    });
  };

  return (
    <form
      noValidate
      autoComplete="off"
      autoCapitalize="off"
      onSubmit={handleSubmit}
      {...props}
    >
      {children}
    </form>
  );
};

export default Form;
