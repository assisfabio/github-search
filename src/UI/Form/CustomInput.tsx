import React, { FunctionComponent, useEffect, useState } from "react";
import {
  InputAdornment,
  TextField,
  TextFieldProps,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";

export type TCustomInput = TextFieldProps & {
  StartIcon?: FunctionComponent;
  EndIcon?: FunctionComponent;
};

export const CustomInput: FunctionComponent<TCustomInput> = (props) => {
  const [config, setConfig] = useState<any>({ value: "" });

  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("sm"));

  const handleChange = (e: any) => {
    config.onChange && config.onChange(e);
  };

  const getInputProps = () => {
    const result: { startAdornment?: any; endAdornment?: any } = {};

    if (config.StartIcon) {
      result.startAdornment = (
        <InputAdornment position="start">
          <config.StartIcon />
        </InputAdornment>
      );
    }

    if (config.EndIcon) {
      result.endAdornment = (
        <InputAdornment position="end">
          <config.EndIcon />
        </InputAdornment>
      );
    }
    return result;
  };

  useEffect(() => {
    setConfig({ ...config, ...props });
    // eslint-disable-next-line
  }, [props]);

  return (
    <>
      <TextField
        size={matches ? "small" : "medium"}
        variant="outlined"
        helperText={config.error && config.helperText}
        onChange={handleChange}
        fullWidth
        InputProps={config && getInputProps()}
        required={config.require}
        type={config.type}
        id={config.id}
        label={config.label}
        error={config.error}
        onBlur={config.onBlur}
        onFocus={config.onFocus}
      />
    </>
  );
};

export default CustomInput;
