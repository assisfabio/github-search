import React, { FunctionComponent } from "react";
import { CssBaseline } from "@material-ui/core";
import { responsiveFontSizes, ThemeProvider } from "@material-ui/core/styles";
import { unstable_createMuiStrictModeTheme as createMuiTheme } from "@material-ui/core";

// Requiring default variables
const defaultTheme = createMuiTheme({});

let theme = createMuiTheme({
  // TODO trazer a troca de Dork/Light
  // palette: isDark ? darkMode : lightMode,
  typography: {
    fontFamily: "inherit",
    h3: {
      alignItems: "center",
      display: "flex",
      fontSize: "1.6em",
      "& svg": {
        marginLeft: ".3em",
      },
    },
  },
  overrides: {
    MuiCssBaseline: {
      "@global": {
        ":root": {
          colorScheme: "light dark",
        },
        "::-webkit-scrollbar": {
          height: "6px !important",
          width: "3px !important",
        },
        "::-webkit-scrollbar-track": {
          background: "transparent",
        },
        "::-webkit-scrollbar-thumb": {
          backgroundColor: "var(--scrollbar-thumb)",
          borderRadius: "3px",
          transition: "all 0.3s ease-in-out",
        },
        "::-webkit-scrollbar-thumb:hover": {
          backgroundColor: "var(--scrollbar-thumb-hover)",
        },
        html: {
          "--scrollbar-thumb": "rgba(0, 0, 0, 0.2)",
          "--scrollbar-thumb-hover": "rgba(0, 0, 0, 0.5)",
          /*
          [defaultTheme.breakpoints.up("sm")]: {
            fontSize: "13px",
          },
          */
        },
        body: {
          fontFamily: `-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
          "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
          sans-serif`,
          WebkitFontSmoothing: "antialiased",
        },
      },
    }, // Endof MuiCssBaseline
    MuiToolbar: {
      gutters: {
        [defaultTheme.breakpoints.up("sm")]: {
          paddingLeft: defaultTheme.spacing(2),
          paddingRight: defaultTheme.spacing(2),
        },
      },
    },
    MuiFormControl: {
      root: {
        marginBottom: defaultTheme.spacing(3),
      },
    },
    MuiFormHelperText: {},
    MuiListItem: {
      gutters: {
        paddingLeft: defaultTheme.spacing(1),
        paddingRight: defaultTheme.spacing(1),
      },
    },
    MuiListItemIcon: {
      root: {
        minWidth: "3.5em",
      },
    },
  },
});
theme = responsiveFontSizes(theme);

const Theme: FunctionComponent = ({ children }) => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};
export default Theme;
