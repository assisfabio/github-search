
export type TDateFormatOptions = Intl.DateTimeFormatOptions & {
  fractionalSecondDigits?: number,
};

export const dateFormat = (date: string | number, options?: TDateFormatOptions): string => {
  const defaultOptions: TDateFormatOptions = {
    day: "2-digit",
    month: "2-digit",
    year: "numeric",
    hour: "numeric",
    minute: "numeric",
    hour12: false,
    timeZone: "America/Sao_Paulo",
  };
  return new Intl.DateTimeFormat('pt-BR', {
    ...defaultOptions,
    ...options,
  }).format(new Date(date));
};
