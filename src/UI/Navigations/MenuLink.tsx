import React, { FunctionComponent } from "react";
import { Link, useRouteMatch } from "react-router-dom";
import { createStyles, ListItem, makeStyles, Theme } from "@material-ui/core";

type TMenuLink = {
  to: string;
  exact?: boolean;
};
const styles = makeStyles((theme: Theme) =>
  createStyles({
    listItem: {
      padding: 0,
    },
    link: {
      alignItems: "center",
      color: "inherit",
      display: "flex",
      padding: theme.spacing(1),
      textDecoration: "none",
      width: "100%",
    },
  })
);

const MenuLink: FunctionComponent<TMenuLink> = ({ to, exact, children }) => {
  let match = useRouteMatch({
    path: to,
    exact: exact,
  });
  const classes = styles();
  return (
    <ListItem selected={Boolean(match)} className={classes.listItem} button>
      <Link to={to} className={classes.link}>
        {children}
      </Link>
    </ListItem>
  );
};

export default MenuLink;
