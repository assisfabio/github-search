import React, { FunctionComponent, useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";

type TPaginatedWraper = {
  total: number;
  perpage?: number;
  startpage?: number;
  onChange: (changes: { page: number; perpage: number }) => void;
};

const styles = makeStyles((theme: Theme) =>
  createStyles({
    wrapper: {
      display: "flex",
      flex: "auto",
      flexDirection: "column",
    },
    container: {},
    header: {
      padding: `${theme.spacing(1)}px 0`,
    },
    footer: {
      padding: `${theme.spacing(1)}px 0`,
      margin: `0 -${theme.spacing(1)}px`,
    },
  })
);

const PaginatedWraper: FunctionComponent<TPaginatedWraper> = ({
  total,
  perpage,
  startpage,
  onChange,
  children,
}) => {
  const [page, setPage] = useState<number>(startpage || 1);
  const [perPage] = useState<number>(perpage || 10);
  const classes = styles();

  const getTotalPages = () => {
    return Math.ceil(total / perPage);
  };

  const handleChange = (e: React.ChangeEvent<unknown>, page: number) => {
    setPage(page);
    onChange({ page, perpage: perPage });
    window.scrollTo(0, 0);
  };

  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        Mostrando página {page} de {getTotalPages()}
      </div>
      <div className={classes.container}>{children}</div>
      <div className={classes.footer}>
        <Pagination
          count={getTotalPages()}
          page={page}
          onChange={handleChange}
        />
      </div>
    </div>
  );
};

export default PaginatedWraper;
