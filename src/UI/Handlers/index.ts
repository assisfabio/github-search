import GlobalLoading from './GlobalLoading';
import NoResults from './NoResults';

export default { GlobalLoading, NoResults };
