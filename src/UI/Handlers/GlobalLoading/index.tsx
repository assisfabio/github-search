import React, { FunctionComponent, useEffect } from "react";
import {
  Backdrop,
  CircularProgress,
  createStyles,
  makeStyles,
  Theme,
} from "@material-ui/core";
import axios from "axios";
import { increment, decrement, count } from "./slice";
import { useDispatch, useSelector } from "react-redux";

const styles = makeStyles((theme: Theme) =>
  createStyles({
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: "#fff",
    },
  })
);

const GlobalLoading: FunctionComponent = () => {
  const classes = styles();
  const dispatch = useDispatch();
  const showLoading = useSelector(count);

  const initAxios = () => {
    axios.interceptors.request.use(
      function (config) {
        dispatch(increment());
        return config;
      },
      function (error) {
        dispatch(decrement());
        return Promise.reject(error);
      }
    );
    axios.interceptors.response.use(
      function (response) {
        dispatch(decrement());
        return response;
      },
      function (error) {
        dispatch(decrement());
        return Promise.reject(error);
      }
    );
  };

  useEffect(() => {
    initAxios();
    // eslint-disable-next-line
  }, []);

  return (
    <Backdrop className={classes.backdrop} open={showLoading > 0}>
      <CircularProgress color="inherit" />
    </Backdrop>
  );
};

export default GlobalLoading;
