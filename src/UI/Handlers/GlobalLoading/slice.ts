import { RootState } from '@Configs/store';
import { createSlice } from '@reduxjs/toolkit';

interface iMenuState {
  count: number;
};

const initialState: iMenuState = {
  count: 0
};

export const loadingSlice = createSlice({
  name: 'menu',
  initialState,
  reducers: {
    increment: state => {
      state.count++;
    },
    decrement: state => {
      state.count--;
    },
  },
});
export const { increment, decrement } = loadingSlice.actions;
export const count = (state: RootState) => state.loading.count;

export default loadingSlice.reducer;
