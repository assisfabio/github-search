import React, { FunctionComponent } from "react";
import {
  Box,
  createStyles,
  makeStyles,
  Theme,
  Typography,
} from "@material-ui/core";
import { SentimentVeryDissatisfiedOutlined } from "@material-ui/icons";

const styles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      alignItems: "center",
      color: theme.palette.grey[300],
      display: "flex",
      flexDirection: "column",
      "& svg": {
        fontSize: "150px",
      },
    },
  })
);

const NoResults: FunctionComponent = () => {
  const classes = styles();
  return (
    <Box className={classes.container}>
      <SentimentVeryDissatisfiedOutlined />
      <Typography variant="button">Nenhum resultado encontrado.</Typography>
      <Typography variant="overline">Faça uma nova busca.</Typography>
    </Box>
  );
};

export default NoResults;
