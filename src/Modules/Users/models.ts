import { TShortUser } from "@Modules/Github/models";
export type THistoryUser = TShortUser & {
  lastview: number;
};