import React, { FunctionComponent, useEffect, useState } from "react";
import {
  Box,
  createStyles,
  Grid,
  makeStyles,
  Theme,
  Typography,
} from "@material-ui/core";
import { useParams } from "react-router-dom";
import { User } from "@Modules/Github/User";
import { TRepository } from "@Modules/Github/models";
import { PaginatedWraper } from "@UI/Navigations";
import UserServices from "./services";
import { CardRepository, ProfileInfo } from "./Components";
import NoResults from "@UI/Handlers/NoResults";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      marginBottom: theme.spacing(2),
      "&:last-child": {
        marginBottom: 0,
      },
    },
  })
);

const UserDetails: FunctionComponent = () => {
  const [user, setUser] = useState<User>();
  const [repositories, setRepositories] = useState<Array<TRepository>>();
  const [pager, setPager] = useState({ page: 1, perpage: 10 });
  const { login } = useParams<{ login: string }>();

  const classes = useStyles();

  const handleGetUser = () => {
    UserServices.getUser(login).then((response) => {
      setUser(new User(response));
    });
  };

  useEffect(() => {
    handleGetUser();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (user?.public_repos) {
      user.getRepositories().then((response) => {
        setRepositories(response);
      });
    }
  }, [user]);

  return (
    <Grid container spacing={3}>
      <Grid item xs={12} sm={6} md={4}>
        {user ? <ProfileInfo source={user} /> : <></>}
      </Grid>
      <Grid item xs={12} sm={6} md={8}>
        <Box>
          <Typography variant="h3">Repositórios</Typography>
          {repositories?.length ? (
            <PaginatedWraper
              total={repositories.length}
              perpage={10}
              onChange={({ page, perpage }) => {
                setPager({ page, perpage });
              }}
            >
              {repositories
                ?.slice(
                  pager.perpage * pager.page - pager.perpage,
                  pager.perpage * pager.page
                )
                .map((item: TRepository, i: number) => (
                  <CardRepository {...item} key={i} className={classes.card} />
                ))}
            </PaginatedWraper>
          ) : (
            <NoResults />
          )}
        </Box>
      </Grid>
    </Grid>
  );
};

export default UserDetails;
