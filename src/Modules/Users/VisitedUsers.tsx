import React, { FunctionComponent, useEffect, useState } from "react";
import { Box, Typography } from "@material-ui/core";
import { THistoryUser } from "@Modules/Users/models";
import UserServices from "@Modules/Users/services";
import { ListShortUsers } from "./Components";
import NoResults from "@UI/Handlers/NoResults";

const VisitedUsers: FunctionComponent = () => {
  const [users, setUsers] = useState<Array<THistoryUser>>();

  const handleGetHistory = () => {
    UserServices.getUserHistory().then((response) => {
      setUsers(response);
    });
  };

  useEffect(() => {
    handleGetHistory();
  }, []);

  return (
    <Box>
      {users?.length ? (
        <>
          <Typography variant="h3">Ultimos usuários visitados</Typography>
          <ListShortUsers source={users} inMemory />
        </>
      ) : (
        <NoResults />
      )}
    </Box>
  );
};

export default VisitedUsers;
