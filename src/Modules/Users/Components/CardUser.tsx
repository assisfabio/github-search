import React, { FunctionComponent, useState } from "react";
import {
  Box,
  Card,
  CardContent,
  CardMedia,
  Typography,
} from "@material-ui/core";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import { Business, FaceOutlined } from "@material-ui/icons";
import { dateFormat } from "@UI/Utils/Date";

import { TShortUser } from "@Modules/Github/models";

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      cursor: "pointer",
      display: "flex",
    },
    details: {
      display: "flex",
      flexDirection: "column",
    },
    content: {
      flex: "1 0 auto",
    },
    cover: {
      maxWidth: 150,
      width: "30%",
    },
    title: {
      display: "flex",
    },
  })
);

type TCardUsers = TShortUser & {
  lastview?: number;
  onClick: () => void;
  className?: string;
};
const CardUsers: FunctionComponent<TCardUsers> = ({
  avatar_url,
  login,
  lastview,
  type,
  onClick,
  className,
}) => {
  const [elevation, setElevation] = useState<number>(1);
  const classes = useStyles();

  return (
    <Card
      className={`${classes.root} ${className || ""}`}
      onClick={onClick}
      elevation={elevation}
      onMouseEnter={() => setElevation(3)}
      onMouseLeave={() => setElevation(1)}
    >
      <CardMedia className={classes.cover} image={avatar_url} title={login} />
      <Box className={classes.details}>
        <CardContent className={classes.content}>
          <Typography component="h5" className={classes.title}>
            {type === "Organization" ? <Business /> : <FaceOutlined />} {login}
          </Typography>
          {lastview && (
            <Typography variant="overline" color="textSecondary">
              {dateFormat(lastview)}
            </Typography>
          )}
        </CardContent>
      </Box>
    </Card>
  );
};

export default CardUsers;
