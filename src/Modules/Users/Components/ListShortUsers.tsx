import React, { FunctionComponent, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { CardUser } from "@Modules/Users/Components";
import { PaginatedWraper } from "@UI/Navigations";
import { TShortUser } from "@Modules/Github/models";
import {
  GridList,
  GridListTile,
  makeStyles,
  createStyles,
  useMediaQuery,
  useTheme,
  Box,
} from "@material-ui/core";

type TListShortUsers = {
  source: Array<TShortUser>;
  total?: number;
  onChange?: (e: any) => void;
  inMemory?: boolean;
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
    },
    gridList: {
      flex: "auto",
      "& .MuiGridListTile-root": {},
    },
    gridListTile: {
      "& .MuiGridListTile-tile": {
        overflow: "visible",
      },
    },
    gridItem: {
      height: "100%",
    },
    icon: {
      color: "rgba(255, 255, 255, 0.54)",
    },
  })
);

const ListShortUsers: FunctionComponent<TListShortUsers> = ({
  source,
  total,
  onChange,
  inMemory,
}) => {
  const [users, setUsers] = useState<typeof source>();
  const [pager, setPager] = useState({ page: 1, perpage: 10 });
  const history = useHistory();
  const classes = useStyles();

  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("sm"));

  const paginate = () => {
    return inMemory
      ? users?.slice(
          pager.perpage * pager.page - pager.perpage,
          pager.perpage * pager.page
        )
      : users;
  };

  useEffect(() => {
    setUsers(source);
  }, [source]);

  return (
    <Box>
      {users?.length && (
        <PaginatedWraper
          total={total || users.length}
          perpage={pager.perpage}
          onChange={({ page, perpage }) => {
            onChange && onChange({ page, perpage });
            setPager({ page, perpage });
          }}
        >
          <Box className={classes.root}>
            <GridList
              cellHeight="auto"
              spacing={theme.spacing(2)}
              className={classes.gridList}
              cols={matches ? 2 : 1}
            >
              {paginate()?.map((user: TShortUser, i: number) => (
                <GridListTile key={i} className={classes.gridListTile}>
                  <CardUser
                    key={i}
                    {...user}
                    onClick={() => history.push(`/users/${user.login}`)}
                    className={classes.gridItem}
                  />
                </GridListTile>
              ))}
            </GridList>
          </Box>
        </PaginatedWraper>
      )}
    </Box>
  );
};

export default ListShortUsers;
