import React, { FunctionComponent, useEffect, useState } from "react";
import {
  Avatar,
  Box,
  Chip,
  createStyles,
  makeStyles,
  Paper,
  Theme,
  Tooltip,
  Typography,
} from "@material-ui/core";
import {
  Business,
  Code,
  FaceOutlined,
  FavoriteBorder,
  GitHub,
  People,
  PersonPinCircleOutlined,
  RoomOutlined,
  Twitter,
  Web,
} from "@material-ui/icons";
import { User } from "@Modules/Github/User";

type TProfileInfo = {
  source: User;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      overflow: "hidden",
    },
    wall: {
      padding: "20%",
      position: "relative",
      width: "100%",
      "& img": {
        filter: "blur(10px) hue-rotate(90deg)",
        left: "50%",
        position: "absolute",
        top: "50%",
        transform: "translate(-50%, -50%)",
      },
    },
    avatar: {
      border: "5px solid #fff",
      boxShadow: theme.shadows[1],
      height: theme.spacing(14),
      left: "50%",
      position: "absolute",
      top: "100%",
      transform: "translate(-50%, -50%)",
      width: theme.spacing(14),
    },
    profile: {
      padding: `${theme.spacing(7)}px ${theme.spacing(2)}px ${theme.spacing(
        2
      )}px`,
      textAlign: "center",
    },
    name: {
      justifyContent: "center",
      wordBreak: "break-all",
      "& .MuiSvgIcon-root": {
        fontSize: "1em",
        margin: "0 .5em 0 0",
      },
    },
    picture: {
      position: "relative",
    },
    social: {
      alignItems: "center",
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "center",
      padding: `${theme.spacing(1)}px 0`,
      "& .MuiChip-root": {
        margin: theme.spacing(1) / 2,
      },
    },
    infos: {},
    icons: {
      display: "inline-block",
    },
  })
);

const ProfileInfo: FunctionComponent<TProfileInfo> = ({ source }) => {
  const [user, setUser] = useState<User>(source);
  const classes = useStyles();

  useEffect(() => {
    if (source) {
      setUser(source);
    }
  }, [source]);
  return (
    <Paper className={classes.container}>
      <Box className={classes.picture}>
        <Avatar
          variant="square"
          src={user?.avatar_url}
          alt={user?.name}
          className={classes.wall}
        />
        <Avatar
          src={user?.avatar_url}
          alt={user?.name}
          className={classes.avatar}
        />
      </Box>
      <Box className={classes.profile}>
        <Typography variant="h3" className={classes.name}>
          {user?.type === "Organization" ? <Business /> : <FaceOutlined />}{" "}
          {user?.name || user?.login}
        </Typography>
        <Box className={classes.social}>
          <Chip
            label={user?.login}
            href={user?.html_url}
            target="_blank"
            clickable
            icon={<GitHub />}
            component="a"
            color="primary"
            size="small"
            variant="outlined"
          />
          {user?.twitter_username && (
            <Chip
              label={user?.twitter_username}
              href={`https://twitter.com/${user?.twitter_username}`}
              target="_blank"
              clickable
              icon={<Twitter />}
              component="a"
              color="secondary"
              size="small"
              variant="outlined"
            />
          )}
          <Tooltip title="Following" arrow placement="top">
            <Chip
              label={user?.following}
              icon={<FavoriteBorder />}
              size="small"
            />
          </Tooltip>
          <Tooltip title="Followers" arrow placement="top">
            <Chip label={user?.followers} icon={<People />} size="small" />
          </Tooltip>
          <Tooltip title="Public repositories" arrow placement="top">
            <Chip label={user?.public_repos} icon={<Code />} size="small" />
          </Tooltip>

          {user?.location && (
            <Tooltip title="Location" arrow placement="top">
              <Chip
                label={user.location}
                icon={
                  user?.type === "Organization" ? (
                    <RoomOutlined />
                  ) : (
                    <PersonPinCircleOutlined />
                  )
                }
                size="small"
                variant="outlined"
              />
            </Tooltip>
          )}

          {user?.blog && (
            <Tooltip title="Web address" arrow placement="top">
              <Chip
                clickable
                component="a"
                href={user.blog}
                target="_blank"
                label={user.blog}
                icon={<Web />}
                size="small"
                variant="outlined"
              />
            </Tooltip>
          )}
          {user?.company && (
            <Tooltip title="Company" arrow placement="top">
              <Chip
                label={user.company}
                icon={<Business />}
                size="small"
                variant="outlined"
              />
            </Tooltip>
          )}
        </Box>
        {user?.bio && <Typography variant="body1">{user.bio}</Typography>}
      </Box>
    </Paper>
  );
};

export default ProfileInfo;
