import React, { FunctionComponent, useState } from "react";
import { Box, Chip, Link, Paper, Typography } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import {
  CallSplit,
  Code,
  DescriptionOutlined,
  StarBorder,
  Visibility,
} from "@material-ui/icons";

import { TRepository } from "@Modules/Github/models";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      cursor: "pointer",
    },
    content: {
      alignItems: "center",
      color: "inherit",
      display: "flex",
      flex: 1,
      justifyContent: "space-between",
      padding: theme.spacing(1),
      "&:hover": {
        textDecoration: "none",
      },
      "& .MuiChip-root": {
        margin: theme.spacing(1) / 2,
      },
      [theme.breakpoints.up("md")]: {
        flexDirection: "row",
      },
      [theme.breakpoints.down("sm")]: {
        flexDirection: "column",
        alignItems: "flex-start",
        "& .MuiTypography-root": {
          borderBottom: `1px solid ${theme.palette.divider}`,
          width: `calc(100% + ${theme.spacing(2)}px)`,
          margin: `0 -${theme.spacing(1)}px ${theme.spacing(1)}px`,
          padding: `0 ${theme.spacing(1)}px ${theme.spacing(1)}px`,
        },
      },
    },
    chips: {
      textAlign: "right",
    },
  })
);

type TCardRepository = TRepository & {
  onClick?: () => void;
  className?: string;
};
const CardRepository: FunctionComponent<TCardRepository> = ({
  name,
  forks_count,
  watchers_count,
  license,
  language,
  stargazers_count,
  html_url,
  onClick,
  className,
}) => {
  const [elevation, setElevation] = useState<number>(1);
  const classes = useStyles();

  return (
    <Paper
      className={`${classes.root} ${className || ""}`}
      onClick={onClick}
      elevation={elevation}
      onMouseEnter={() => setElevation(3)}
      onMouseLeave={() => setElevation(1)}
    >
      <Link href={html_url} target="_blank" className={classes.content}>
        <Typography variant="body1">{name}</Typography>

        <Box className={classes.chips}>
          <Chip label={stargazers_count} icon={<StarBorder />} size="small" />
          <Chip label={watchers_count} icon={<Visibility />} size="small" />
          <Chip label={forks_count} icon={<CallSplit />} size="small" />
          {language && <Chip label={language} icon={<Code />} size="small" />}
          {license && (
            <Chip
              label={license.key}
              icon={<DescriptionOutlined />}
              size="small"
            />
          )}
        </Box>
      </Link>
    </Paper>
  );
};

export default CardRepository;
