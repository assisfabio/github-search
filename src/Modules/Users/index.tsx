import UserDetails from "./Details";
import VisitedUsers from "./VisitedUsers";

export { UserDetails, VisitedUsers };
