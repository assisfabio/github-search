import axios from "axios";
import { TGetUserResponse, TGetUsersResponse, TShortUser } from "@Modules/Github/models";
import { buildGetUserParams, buildSearchUsersParams, Urls } from "@Modules/Github/configs";
import SearchServices from "@Modules/Search/services";
import { THistoryUser } from "./models";

export interface iUserServices {
  getUser: (term: string) => Promise<TGetUserResponse>;
  getUsers: (config: {term: string, page?: number, per_page?: number}) => Promise<TGetUsersResponse>;
  getUserHistory: (delay?: number) => Promise<Array<THistoryUser>>;
  setUserToHistory: (user: TShortUser) => Promise<any>;
};

const UserServices: iUserServices = {
  getUser: async (term: string) => {
    return axios.get(`${Urls.baseGetUserUrl(term)}`, buildGetUserParams())
    .then(response => {
      UserServices.setUserToHistory(response.data); // Simulate backend record history
      return response.data;
    })
    .catch(error => error)
  },
  getUsers: async ({term, page, per_page}) => {
    return axios.get(`${Urls.baseSearchUsersUrl}`, buildSearchUsersParams({term, page: page || 1, per_page: per_page || 10}))
    .then(response => {
      const data = response.data;

      /**
       * Simulate a backend record history
       * It`s poor, I know, but should be a backend job...
       * - This is the way
       */
      SearchServices.getLastSearchedTerm().then(lastSearched => {
        if ((!page || page === 1) && term !== lastSearched) {
          SearchServices.setItemToHistory({
            term: term,
            timestamp: new Date().getTime(),
            total: data?.total_count,
            incomplete: data?.incomplete_results,
            firstResults: data.items?.slice(0, 5).map((item: TShortUser) => 
              ({avatar: item.avatar_url, login: item.login})),
          }); 
        }
      })

      return response.data;
    })
    .catch(error => {
      console.log('Request error:', error);

      /**
       * - This is the way, Bro!
       */
      if (!page || page === 1) {
        SearchServices.setItemToHistory({
          term: term,
          timestamp: new Date().getTime(),
          total: 0,
          error: true,
        });
      }
    })
  },
  getUserHistory: async (delay) => {
    return new Promise((resolve, reject) => {
      window.setTimeout(() => {
        try {
          const history = JSON.parse(localStorage.getItem('user-history') || '[]');
          /**
           * For larger projects, functions like the "sort" below may be more customizable,
           *  but in a project like this, this is unnecessary.
           */
          resolve(history.sort((a: THistoryUser, b: THistoryUser) => {
            if ( a.lastview < b.lastview ){
              return 1;
            }
            if ( a.lastview > b.lastview ){
              return -1;
            }
            return 0;
          }));
        } catch (error) {
          reject(error);
        }
      }, delay || 0);
    });
  },
  setUserToHistory: async (user) => {
    // Simulate a backend endpoint
    return new Promise((resolve) => {
      const users = JSON.parse(localStorage.getItem('user-history') || '[]');
      const index = users.findIndex((item: THistoryUser) => item.login === user.login);

      if (index >= 0) {
        users[index].lastview = new Date().getTime()
      } else {
        users.push({
          ...user,
          lastview: new Date().getTime(),
        });
      }
      localStorage.setItem('user-history', JSON.stringify(users));
      resolve(true);
    });
  },
};

export default UserServices;
