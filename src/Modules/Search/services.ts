import { THistoryItem } from "./models";

export interface iSearchServices {
  setItemToHistory: (result: THistoryItem) => Promise<any>;
  getSearchHistory: (delay?: number) => Promise<Array<THistoryItem>>;
  getLastSearchedTerm: () => Promise<string>;
};

const SearchServices: iSearchServices = {
  setItemToHistory: async (result) => {
    // Simulate a backend endpoint
    return new Promise((resolve) => {
      const history = JSON.parse(localStorage.getItem('search-history') || '[]');
      history.push(result);
      localStorage.setItem('search-history', JSON.stringify(history));
      resolve(true);
    });
  },
  getSearchHistory: async (delay) => {
    return new Promise((resolve, reject) => {
      window.setTimeout(() => {
        try {
          const history = JSON.parse(localStorage.getItem('search-history') || '[]');
          /**
           * For larger projects, functions like the "sort" below may be more customizable,
           *  but in a project like this, this is unnecessary.
           */
          resolve(history.sort((a: THistoryItem, b: THistoryItem) => {
            if ( a.timestamp < b.timestamp ){
              return 1;
            }
            if ( a.timestamp > b.timestamp ){
              return -1;
            }
            return 0;
          }));
        } catch (error) {
          reject(error);
        }
      }, delay || 0);
    });
  },
  getLastSearchedTerm: async () => {
    return new Promise((resolve, reject) => {
      try {
        SearchServices.getSearchHistory().then(response => {
          if (response?.length) {
            resolve(response[0].term);
          }
          resolve('');
        });
      } catch (error) {
        console.log(error);
        reject(error);
      }
    });
  },
};

export default SearchServices;