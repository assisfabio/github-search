import React, { FunctionComponent, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import {
  Box,
  Collapse,
  Grid,
  Tooltip,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { ErrorOutline, Search } from "@material-ui/icons";
import { TGetUsersResponse } from "@Modules/Github/models";
import { VisitedUsers } from "@Modules/Users";
import UserServices from "@Modules/Users/services";
import { ListShortUsers } from "@Modules/Users/Components";
import { handlePlurals } from "@UI/Utils/Strings";
import { Button, Form, Username } from "@UI/Form";

import SearchServices from "./services";
import { searchStyles } from "./styles";
import { ListTermHistory } from "./Components";
import NoResults from "@UI/Handlers/NoResults";

const Dashboard: FunctionComponent = () => {
  const [searchs, setSearchs] = useState<any>();
  const [result, setResult] = useState<TGetUsersResponse>();
  const [error, setError] = useState({ username: false });
  const [collapsed, setCollapsed] = useState<boolean>();
  const classes = searchStyles();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("md"));

  const history = useHistory();
  const { term } = useParams<{ term: string }>();

  const handleGetHistory = () => {
    SearchServices.getSearchHistory().then((response) => {
      setSearchs(response);
    });
  };

  const handleGetUsers = (page?: number, per_page?: number) => {
    UserServices.getUsers({ term, page, per_page }).then((response) => {
      setResult(response);
      handleGetHistory();
    });
  };

  const handleSearch = (search?: string) => {
    history.push(`/search/${search}`);
  };

  const handleSubmit = (e: any, data: any) => {
    const username = data.fields.find((item: any) => item.id === "username")
      ?.value;

    document.getElementById("username")?.blur();

    if (data.valid && username) {
      handleSearch(username);
    } else {
      setError({ username: !username });
    }
  };

  useEffect(() => {
    handleGetHistory();
  }, []);

  useEffect(() => {
    if (term) {
      handleGetUsers();
    } else {
      setResult(undefined);
    }
    // eslint-disable-next-line
  }, [term]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={12} md={5}>
        <Form onSubmit={handleSubmit} className={classes.searchForm}>
          <Username
            label="Digite um nome de usuário"
            id="username"
            error={error.username}
            onFocus={() => {
              setCollapsed(true);
            }}
            onBlur={() => window.setTimeout(() => setCollapsed(false), 500)}
          />
          <Button
            type="submit"
            color="primary"
            aria-label="Buscar"
            className={classes.submitBtn}
          >
            <Search />
          </Button>
        </Form>

        <Collapse in={matches || collapsed}>
          <Typography variant="h3">Histórico de buscas</Typography>
          <ListTermHistory
            source={searchs}
            onChange={() =>
              window.setTimeout(() => {
                document.getElementById("username")?.focus();
              }, 500)
            }
          />
        </Collapse>
      </Grid>

      <Grid item xs={12} sm={12} md={7}>
        {term ? (
          <Box>
            {result?.items?.length ? (
              <>
                <Typography variant="h3">
                  {`Encontramos ${result.total_count} resultado${handlePlurals(
                    result.total_count
                  )}`}
                  {result.incomplete_results && (
                    <Tooltip
                      title="Os resultados podem estar incompletos"
                      arrow
                      placement="top"
                    >
                      <ErrorOutline color="error" />
                    </Tooltip>
                  )}
                </Typography>
                <ListShortUsers
                  source={result.items}
                  total={result.total_count}
                  onChange={({ page, perpage }) =>
                    handleGetUsers(page, perpage)
                  }
                />
              </>
            ) : (
              <NoResults />
            )}
          </Box>
        ) : (
          <VisitedUsers />
        )}
      </Grid>
    </Grid>
  );
};

export default Dashboard;
