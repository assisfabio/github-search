import { createStyles, makeStyles, Theme } from "@material-ui/core";

export const searchStyles = makeStyles((theme: Theme) =>
  createStyles({
    searchForm: {
      alignItems: 'flex-start',
      display: 'flex',
    },
    submitBtn: {
      marginLeft: theme.spacing(1),
      marginTop: '1px',
      minWidth: 'auto',
      padding: theme.spacing(1),
    }
  })
);
