import React, { FunctionComponent, useState } from "react";
import {
  Avatar,
  Box,
  Chip,
  createStyles,
  Divider,
  makeStyles,
  Paper,
  Theme,
  Tooltip,
  Typography,
} from "@material-ui/core";
import { dateFormat } from "@UI/Utils/Date";
import { THistoryItem } from "../models";
import { useHistory } from "react-router-dom";
import { BugReportOutlined, ErrorOutline } from "@material-ui/icons";
import { handlePlurals } from "@UI/Utils/Strings";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
      color: theme.palette.text.secondary,
      cursor: "pointer",
      marginBottom: theme.spacing(2),
      transition: "all .3s ease-in-out",
      "&:hover": {
        color: "#000",
      },
    },
    term: {
      alignItems: "center",
      display: "flex",
      justifyContent: "space-between",
      wordBreak: "break-word",
      "& small": {
        fontSize: ".8em",
      },
    },
    infos: {
      alignItems: "center",
      display: "flex",
      justifyContent: "space-between",
    },
    chips: {
      margin: theme.spacing(1) / 2,
    },
    divider: {
      marginBottom: theme.spacing(1),
    },
  })
);

const CardHistoryItem: FunctionComponent<
  THistoryItem & { onClick?: () => void }
> = ({ term, timestamp, total, error, firstResults, incomplete, onClick }) => {
  const [elevation, setElevation] = useState<number>(1);
  const history = useHistory();
  const classes = useStyles();

  return (
    <Paper
      className={classes.paper}
      elevation={elevation}
      onMouseEnter={() => setElevation(3)}
      onMouseLeave={() => setElevation(1)}
      onClick={onClick}
    >
      <Box>
        <Typography variant="overline" className={classes.infos}>
          <strong>{dateFormat(timestamp)}</strong>
          <strong>
            {(incomplete || error) && (
              <span>
                {incomplete && (
                  <Tooltip title="Houve um erro na requisição. Os resultados podem estar incompletos">
                    <ErrorOutline />
                  </Tooltip>
                )}
                {error && (
                  <Tooltip title="Houve um erro nessa tentaiva">
                    <BugReportOutlined />
                  </Tooltip>
                )}
              </span>
            )}
            {total} resultado{handlePlurals(total)}
          </strong>
        </Typography>
        <Typography variant="h5" gutterBottom className={classes.term}>
          <span>"{term}"</span>
        </Typography>
      </Box>

      {firstResults && firstResults?.length > 0 ? (
        <>
          <Divider className={classes.divider} />
          {firstResults.map((user: any, index: number) => (
            <Chip
              className={classes.chips}
              key={index}
              size="small"
              avatar={<Avatar alt={user.login} src={user.avatar} />}
              label={user.login}
              onClick={(e: any) => {
                e.stopPropagation();
                history.push(`/users/${user.login}`);
              }}
            />
          ))}
          {total > firstResults.length && (
            <Chip
              className={classes.chips}
              size="small"
              label={`e mais ${total - firstResults.length} usuarios... `}
              onClick={onClick}
            />
          )}
        </>
      ) : (
        <></>
      )}
    </Paper>
  );
};

export default CardHistoryItem;
