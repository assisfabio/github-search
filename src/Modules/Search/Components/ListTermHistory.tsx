import React, { FunctionComponent, useEffect, useState } from "react";
import { Box } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { PaginatedWraper } from "@UI/Navigations";
import { THistoryItem } from "../models";
import CardHistoryItem from "./CardHistoryItem";
import NoResults from "@UI/Handlers/NoResults";

type TListTermHistory = {
  source: Array<THistoryItem>;
  total?: number;
  onChange?: (e: any) => void;
};

const ListTermHistory: FunctionComponent<TListTermHistory> = ({
  source,
  total,
  onChange,
}) => {
  const [terms, setUsers] = useState<typeof source>();
  const history = useHistory();
  const [pager, setPager] = useState({ page: 1, perpage: 5 });

  const handleSearch = (search?: string) => {
    history.push(`/search/${search}`);
  };

  useEffect(() => {
    setUsers(source);
  }, [source]);

  return (
    <Box>
      {terms?.length ? (
        <PaginatedWraper
          total={total || terms.length}
          perpage={pager.perpage}
          onChange={(e) => {
            setPager(e);
            onChange && onChange(e);
          }}
        >
          {terms
            ?.slice(
              pager.perpage * pager.page - pager.perpage,
              pager.perpage * pager.page
            )
            .map((item: THistoryItem, i: number) => (
              <CardHistoryItem
                key={i}
                onClick={() => handleSearch(item.term)}
                {...item}
              />
            ))}
        </PaginatedWraper>
      ) : (
        <NoResults />
      )}
    </Box>
  );
};

export default ListTermHistory;
