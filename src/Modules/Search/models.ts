export type THistoryItem = {
  term: string,
  timestamp: number;
  total: number;
  incomplete?: boolean;
  error?: boolean;
  firstResults?: Array<{
    avatar: string;
    label: string;
  }>
};
