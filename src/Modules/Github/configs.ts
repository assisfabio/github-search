import { AxiosRequestConfig } from "axios";
import {
  TUserSearchParamOrder,
  TUserSearchParams,
  TUserSearchParamSort,
} from "./models";

export const baseUrl = "https://api.github.com";
export const baseSearchUrl = `${baseUrl}/search`;
export const baseSearchUsersUrl = `${baseSearchUrl}/users`;
export const baseGetUserUrl = (term: string): string =>
  `${baseUrl}/users/${term}`;

export const Urls = {
  baseUrl,
  baseSearchUrl,
  baseSearchUsersUrl,
  baseGetUserUrl,
};

export const buildGetUserParams = () => {
  return {
    headers: {
      accept: "application/vnd.github.v3+json",
    }
  };
}

export const buildSearchUsersParams = ({
  term,
  sort,
  order,
  per_page,
  page
}: {
  term: string,
  sort?: TUserSearchParamSort,
  order?: TUserSearchParamOrder,
  per_page?: number,
  page?: number
}): AxiosRequestConfig => {
  const params: TUserSearchParams = {
    q: `${term} in:login`,
    sort: sort,
    order: order,
    per_page: per_page,
    page: page,
  };
  return {
    ...buildGetUserParams(),
    params: params,
  };
};
