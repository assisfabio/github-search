export type TUserSearchParamSort = "followers" | "repositories" | "joined";
export type TUserSearchParamOrder = "desc" | "asc";

export type TUserSearchParams = {
  q: string;
  sort?: TUserSearchParamSort;
  order?: TUserSearchParamOrder;
  per_page?: number;
  page?: number;
};

export type TShortUser = {
  avatar_url: string;
  events_url: string;
  followers_url: string;
  following_url: string;
  gists_url: string;
  gravatar_id: string;
  html_url: string;
  id: number;
  login: string;
  node_id: string;
  organizations_url: string;
  received_events_url: string;
  repos_url: string;
  score: number;
  site_admin: boolean;
  starred_url: string;
  subscriptions_url: string;
  type: string;
  url: string;
};

export type TLongUser = TShortUser & {
  bio: string | null;
  blog: string | null;
  company: string | null;
  created_at: string;
  email: string | null;
  followers: number;
  following: number;
  hireable: null;
  id: number;
  location: string;
  name: string;
  public_gists: number;
  public_repos: number;
  twitter_username: string;
  updated_at: string;
};

export type TGetUsersResponse = {
  incomplete_results: boolean;
  items: Array<TShortUser>;
  total_count: number;
};

export type TGetUserResponse = TLongUser;

export type TRepository = {
  assignees_url: string;
  archived: boolean;
  archive_url: string;
  blobs_url: string;
  branches_url: string;
  clone_url: string;
  collaborators_url: string;
  comments_url: string;
  commits_url: string;
  compare_url: string;
  contents_url: string;
  contributors_url: string;
  created_at: string;
  default_branch: string;
  deployments_url: string;
  description: string | null;
  disabled: boolean;
  downloads_url: string;
  events_url: string;
  fork: boolean;
  forks: number;
  forks_count: number;
  forks_url: string;
  full_name: string;
  git_commits_url: string;
  git_refs_url: string;
  git_tags_url: string;
  git_url: string;
  has_downloads: boolean;
  has_issues: boolean;
  has_pages: boolean;
  has_projects: boolean;
  has_wiki: boolean;
  homepage: string | null;
  hooks_url: string;
  html_url: string;
  id: number;
  issue_comment_url: string;
  issue_events_url: string;
  issues_url: string;
  keys_url: string;
  labels_url: string;
  language: string;
  languages_url: string;
  license: {
    key: string;
    name: string;
    node_id: string;
    spdx_id: string;
    url: string;
  };
  merges_url: string;
  milestones_url: string;
  mirror_url: string | null;
  name: string;
  node_id: string;
  notifications_url: string;
  open_issues_count: number;
  open_issues: number;
  owner: TShortUser;
  private: boolean;
  pulls_url: string;
  pushed_at: string;
  releases_url: string;
  stargazers_url: string;
  statuses_url: string;
  subscribers_url: string;
  subscription_url: string;
  tags_url: string;
  teams_url: string;
  trees_url: string;
  size: number;
  ssh_url: string;
  stargazers_count: number;
  svn_url: string;
  updated_at: string;
  url: string;
  watchers: number;
  watchers_count: number;
};

export interface iUser extends TLongUser {
  getRepositories: () => Promise<Array<TRepository>>;
};
