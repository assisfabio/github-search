import axios from "axios";
import { TRepository } from "./models";

export interface iGithubServices {
  getRepositories: (url: string) => Promise<Array<TRepository>>;
};

const GithubServices: iGithubServices = {
  getRepositories: (url) => {
    return axios.get(url)
    .then(response => response.data)
    .catch(error => error);
  }
};

export default GithubServices;