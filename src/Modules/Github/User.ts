import { iUser, TLongUser } from "./models";
import GithubServices from "./services";

export class User implements iUser {
  avatar_url: string;
  events_url: string;
  followers_url: string;
  following_url: string;
  gists_url: string;
  gravatar_id: string;
  html_url: string;
  id: number;
  login: string;
  node_id: string;
  organizations_url: string;
  received_events_url: string;
  repos_url: string;
  score: number;
  site_admin: boolean;
  starred_url: string;
  subscriptions_url: string;
  type: string;
  url: string;
  bio: string | null;
  blog: string | null;
  company: string | null;
  created_at: string;
  email: string | null;
  followers: number;
  following: number;
  hireable: null;
  location: string;
  name: string;
  public_gists: number;
  public_repos: number;
  twitter_username: string;
  updated_at: string;

  constructor(user: TLongUser) {
    this.avatar_url = user.avatar_url;
    this.events_url = user.events_url;
    this.followers_url = user.followers_url;
    this.following_url = user.following_url;
    this.gists_url = user.gists_url;
    this.gravatar_id = user.gravatar_id;
    this.html_url = user.html_url;
    this.id = user.id;
    this.login = user.login;
    this.node_id = user.node_id;
    this.organizations_url = user.organizations_url;
    this.received_events_url = user.received_events_url;
    this.repos_url = user.repos_url;
    this.score = user.score;
    this.site_admin = user.site_admin;
    this.starred_url = user.starred_url;
    this.subscriptions_url = user.subscriptions_url;
    this.type = user.type;
    this.url = user.url;
    this.bio = user.bio;
    this.blog = user.blog;
    this.company = user.company;
    this.created_at = user.created_at;
    this.email = user.email;
    this.followers = user.followers;
    this.following = user.following;
    this.hireable = user.hireable;
    this.location = user.location;
    this.name = user.name;
    this.public_gists = user.public_gists;
    this.public_repos = user.public_repos;
    this.twitter_username = user.twitter_username;
    this.updated_at = user.updated_at;
  }

  getRepositories() {
    return GithubServices.getRepositories(this.repos_url);
  }
};
