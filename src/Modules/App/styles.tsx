import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import { fade } from "@material-ui/core/styles/colorManipulator";
import { PaletteOptions } from "@material-ui/core/styles/createPalette";

export const lightMode: PaletteOptions = {
  primary: {
    // light: será calculada com base em palette.primary.main,
    main: "#ff4400",
    // dark: será calculada com base em palette.primary.main,
    // contrastText: será calculada para contrastar com palette.primary.main
  },
};

export const darkMode: PaletteOptions = {
  primary: {
    // light: será calculada com base em palette.primary.main,
    main: "#990000",
    // dark: será calculada com base em palette.primary.main,
    // contrastText: será calculada para contrastar com palette.primary.main
  },
  background: {
    paper: "#000",
    default: "#333",
  },
  text: {
    primary: "rgba(255, 255, 255, 0.87)",
    secondary: "rgba(255, 255, 255, 0.54)",
    disabled: "rgba(255, 255, 255, 0.38)",
    hint: "rgba(255, 255, 255, 0.38)",
  },
};

const imageIds = ["449", "110", "31", "35", "362", "380", "390", "399", "402"];
const getRandomInt = (min: number, max: number): number => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
};

const rootDefault = (theme: Theme) => ({
  backgroundColor: theme.palette.background.default,
  display: "flex",
  minHeight: "100vh",
  "&::before": {
    background: `center url(https://picsum.photos/id/${
      imageIds[getRandomInt(0, imageIds.length - 1)]
    }/1080/1920?blur) no-repeat`,
    backgroundSize: "cover",
    content: '""',
    height: "45%",
    maxHeight: "500px",
    position: "absolute",
    width: "100%",
    zIndex: 1,
  },
});

export const publicStylesDesktop = makeStyles((theme: Theme) =>
  createStyles({
    root: rootDefault(theme),
    content: {
      backgroundColor: theme.palette.background.paper,
      borderRadius: "5px",
      boxShadow: "0px 0px 5px 1px rgba(100, 100, 100, 0.5)",
      left: "calc(50% + 23vw)",
      minWidth: "20rem",
      padding: theme.spacing(3),
      position: "absolute",
      top: theme.spacing(6),
      transform: "translate(-50%, 0%)",
      zIndex: 2,
    },
  })
);
export const publicStylesMobile = makeStyles((theme: Theme) =>
  createStyles({
    root: rootDefault(theme),
    content: {
      backgroundColor: fade(theme.palette.background.paper, 0.8),
      padding: theme.spacing(3),
      position: "relative",
      textAlign: "center",
      width: "100%",
      zIndex: 2,
    },
  })
);

export const privateStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  })
);
