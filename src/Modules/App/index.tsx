import React, { FunctionComponent } from "react";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import {
  Container,
  List,
  ListItemIcon,
  ListItemText,
  Toolbar,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import {
  AssignmentInd as AssignmentIndIcon,
  SearchRounded as SearchRoundedIcon,
} from "@material-ui/icons";

import { Menu, TopBar } from "./Components/index";
import { Search, UserDetails } from "@Modules/index";
import { VisitedUsers } from "@Modules/Users";

import { MenuLink } from "@UI/Navigations";
import { privateStyles } from "./styles";
import GlobalLoading from "@UI/Handlers/GlobalLoading";

const App: FunctionComponent = () => {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("sm"));
  const classes = privateStyles();

  return (
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <div className={classes.root}>
        <TopBar />
        <Menu>
          <List>
            {[
              {
                Icon: SearchRoundedIcon,
                to: "/search",
                label: "Busca",
                exact: false,
              },
              {
                Icon: AssignmentIndIcon,
                to: "/users",
                label: "Usuários Visitados",
              },
            ].map(({ Icon, to, label, exact }, index) => (
              <React.Fragment key={index}>
                <MenuLink to={to} key={index} exact={exact}>
                  <ListItemIcon>
                    <Icon />
                  </ListItemIcon>
                  <ListItemText primary={label} />
                </MenuLink>
              </React.Fragment>
            ))}
          </List>
        </Menu>
        <main className={classes.content}>
          <Toolbar variant={matches ? "dense" : "regular"} />
          <Container maxWidth="md">
            <Switch>
              <Route component={Search} path="/search" exact />
              <Route component={Search} path="/search/:term" exact />
              <Route component={VisitedUsers} path="/users" exact />
              <Route component={UserDetails} path="/users/:login" exact />
              <Route path="*">
                <Redirect to="/search" />
              </Route>
            </Switch>
          </Container>
        </main>
      </div>
      <GlobalLoading />
    </BrowserRouter>
  );
};

export default App;
