import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";

export const topBarStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    menuButton: {
      marginRight: theme.spacing(1),
    },
    appTitle: {
      flexGrow: 1,
      letterSpacing: theme.typography.fontSize / 10,
      marginLeft: theme.spacing(1),
    },
    appSubtitle: {
      fontWeight: 300,
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(2),
    },
  })
);
