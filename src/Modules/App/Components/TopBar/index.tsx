import React, { FunctionComponent } from "react";
import {
  AppBar,
  IconButton,
  Toolbar,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { GitHub, Menu as MenuIcon, MenuOpen } from "@material-ui/icons";
import { useSelector, useDispatch } from "react-redux";
import { menuOpened, toggleOpenMenu } from "@Modules/App/Components/Menu/slice";

import { topBarStyles } from "./styles";

const TopBar: FunctionComponent = ({ children }) => {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("sm"));
  const classes = topBarStyles();
  const isMenuOpened = useSelector(menuOpened);

  const dispatch = useDispatch();

  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar variant={matches ? "dense" : "regular"}>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={() => dispatch(toggleOpenMenu())}
          className={classes.menuButton}
        >
          {isMenuOpened ? <MenuOpen /> : <MenuIcon />}
        </IconButton>
        <GitHub />
        <Typography variant="h6" noWrap className={classes.appTitle}>
          Github!
          <span className={classes.appSubtitle}>Search</span>
        </Typography>

        {children}
      </Toolbar>
    </AppBar>
  );
};

export default TopBar;
