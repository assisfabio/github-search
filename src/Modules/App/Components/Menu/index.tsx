import React, { FunctionComponent, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import clsx from "clsx";
import {
  Divider,
  Drawer,
  IconButton,
  Toolbar,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import MenuOpen from "@material-ui/icons/MenuOpen";
import { useLocation } from "react-router-dom";

import { menuOpened, toggleOpenMenu, closeMenu } from "./slice";
import { menuStyles } from "./styles";

const Menu: FunctionComponent = ({ children }) => {
  const [mouseOver, setMouseOver] = useState<boolean>(false);

  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("sm"));
  const classes = menuStyles();
  const dispatch = useDispatch();
  const isMenuOpened = useSelector(menuOpened);
  const location = useLocation();

  const handleMouseOver = (value: boolean): void => {
    setMouseOver(value);
  };

  useEffect(() => {
    dispatch(closeMenu());
    // eslint-disable-next-line
  }, [location.pathname]);

  return (
    <Drawer
      open={isMenuOpened}
      onClose={() => dispatch(closeMenu())}
      variant={matches ? "permanent" : "temporary"}
      className={clsx(classes.drawer, {
        [classes.drawerOpen]: isMenuOpened,
        [classes.drawerClose]: !isMenuOpened,
      })}
      classes={{
        paper: clsx({
          [classes.drawerOpen]: isMenuOpened,
          [classes.drawerClose]: !isMenuOpened,
          [classes.drawerOver]: mouseOver,
        }),
      }}
    >
      <Toolbar
        variant={matches ? "dense" : "regular"}
        className={classes.drawerHeader}
      >
        <IconButton
          color="inherit"
          aria-label="Abrir Menu"
          edge="start"
          onClick={() => dispatch(toggleOpenMenu())}
        >
          {isMenuOpened ? <MenuOpen /> : <MenuIcon />}
        </IconButton>
      </Toolbar>
      <Divider />
      <div
        className={classes.drawerContainer}
        onMouseEnter={() => handleMouseOver(true)}
        onMouseLeave={() => handleMouseOver(false)}
      >
        {children}
      </div>
    </Drawer>
  );
};
export default Menu;
