import { RootState } from '@Configs/store';
import { createSlice } from '@reduxjs/toolkit';

interface iMenuState {
  open: boolean;
};

const initialState: iMenuState = {
  open: false
};

export const menuSlice = createSlice({
  name: 'menu',
  initialState,
  reducers: {
    toggleOpenMenu: state => {
      state.open = !state.open;
    },
    openMenu: state => {
      state.open = true;
    },
    closeMenu: state => {
      state.open = false;
    },
  },
});
export const { toggleOpenMenu, openMenu, closeMenu } = menuSlice.actions;
export const menuOpened = (state: RootState) => state.menu.open;

export default menuSlice.reducer;
