import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import menuReducer from '@Modules/App/Components/Menu/slice';
import loadingReducer from '@UI/Handlers/GlobalLoading/slice';

export const store = configureStore({
  reducer: {
    loading: loadingReducer, 
    menu: menuReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
