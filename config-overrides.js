const path = require('path');
const paths = require('./tsconfig.paths.json').compilerOptions?.paths;
const alias = {};

Object.keys(paths).map(key => {
  alias[key.replace('/*', '')] = path.resolve(__dirname, paths[key][0].replace('./src', 'src').replace('/*', ''));
});

module.exports = function override(config) {
  config.resolve = {
    ...config.resolve,
    alias: {
      ...config.alias,
      ...alias,
    },
  };
  return config;
};